# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4), ("Rouge-gorge",2)
            ]

# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieurs on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if liste_observations==[]:
        return None
    oiseau_max=-1
    for i in range (len(liste_observations)):
        if liste_observations[i][1]>oiseau_max:
            oiseau_max=liste_observations[i][1]
            imax=i
    return liste_observations[imax][0]

#Ex 2.1
def recherche_oiseau(nom,liste_oiseaux):
    """Permet de retrouver les caractéristiques (nom,famille) d’un oiseau dans une liste d’oiseaux à partir de son nom

    Args:
        nom (str): Nom de l'oiseau

    Returns:
        tuple : Tuple (nom,famille)
    """    
    for i in range(len(liste_oiseaux)):
        if liste_oiseaux[i][0]==nom:
            return liste_oiseaux[i]
    return None

#Ex 2.2
def recherche_par_famille(famille,liste_oiseaux):
    """Permet de retrouver tous les oiseaux d’une même famille dans une liste d’oiseaux

    Args:
        famille (str): Famille d'oiseau

    Returns:
        list: Liste de tuples contenant le nom et la famille de l'oiseau
    """    
    liste=[]
    for i in range(len(liste_oiseaux)):
        if liste_oiseaux[i][1]==famille:
            liste.append(liste_oiseaux[i])
    return liste

#3.1

def est_liste_observations(liste):
    
#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)